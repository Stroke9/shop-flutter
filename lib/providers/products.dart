import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;

import '../models/http_exception.dart';

class Product with ChangeNotifier {
  final String id;
  final String title;
  final String description;
  final String imageUrl;
  final double price;
  bool isFavorite;

  Product({
    @required this.id,
    @required this.title,
    @required this.description,
    @required this.imageUrl,
    @required this.price,
    this.isFavorite = false,
  });

  void _setFavStatus(bool status) {
    isFavorite = status;
    notifyListeners();
  }

  Future<void> toggleFavoriteStatus(String id) async {
    var url = 'https://shop-flutter-3fb65.firebaseio.com/product/$id.json';
    var _oldStatus = isFavorite;

    _setFavStatus(!isFavorite);
    try {
      final res = await http.patch(
        url,
        body: json.encode({
          'isFavorite': isFavorite,
        }),
      );
      if (res.statusCode >= 400) {
        _setFavStatus(_oldStatus);
        throw HttpException('${res.statusCode}: Impossible to add in favortie');
      }
    } catch (err) {
      _setFavStatus(_oldStatus);
    }
  }
}

class Products with ChangeNotifier {
  List<Product> _items = [
    /* Product(
      id: 'p1',
      title: 'Red Shirt',
      description: 'A red shirt - it is pretty red!',
      price: 29.99,
      imageUrl: 'https://cdn.pixabay.com/photo/2016/10/02/22/17/red-t-shirt-1710578_1280.jpg',
    ),
    Product(
      id: 'p2',
      title: 'Trousers',
      description: 'A nice pair of trousers.',
      price: 59.99,
      imageUrl: 'https://upload.wikimedia.org/wikipedia/commons/thumb/e/e8/Trousers%2C_dress_%28AM_1960.022-8%29.jpg/512px-Trousers%2C_dress_%28AM_1960.022-8%29.jpg',
    ),
    Product(
      id: 'p3',
      title: 'Yellow Scarf',
      description: 'Warm and cozy - exactly what you need for the winter.',
      price: 19.99,
      imageUrl: 'https://live.staticflickr.com/4043/4438260868_cc79b3369d_z.jpg',
    ),
    Product(
      id: 'p4',
      title: 'A Pan',
      description: 'Prepare any meal you want.',
      price: 49.99,
      imageUrl: 'https://upload.wikimedia.org/wikipedia/commons/thumb/1/14/Cast-Iron-Pan.jpg/1024px-Cast-Iron-Pan.jpg',
    ), */
  ];

  List<Product> get items {
    return [..._items];
  }

  List<Product> get showFavoriteProduct {
    return _items.where((product) => product.isFavorite).toList();
  }

  Product findById(String id) {
    return _items.firstWhere((product) => product.id == id);
  }

  Future<void> fetchAndSetProduct() async {
    const url = "https://shop-flutter-3fb65.firebaseio.com/product.json";
    final List<Product> loadedProduct = [];

    try {
      final res = await http.get(url);
      final extractedData = json.decode(res.body) as Map<String, dynamic>;
      if(extractedData == null) {
        return;
      }
      extractedData.forEach((productId, productData) {
        loadedProduct.add(Product(
          id: productId,
          title: productData['title'],
          description: productData['description'],
          price: productData['price'],
          imageUrl: productData['imageUrl'],
          isFavorite: productData['isFavorite'],
        ));
      });

      _items = loadedProduct;
      notifyListeners();
    } catch (err) {
      print(err);
      throw (err);
    }
  }

  Future<void> addProduct(Product product) async {
    const url = "https://shop-flutter-3fb65.firebaseio.com/product.json"; // a mettre dans un fichier a part et concontainer l'url souhaite : ${url}/product.json

    try {
      final response = await http.post(
        url,
        body: json.encode({
          'title': product.title,
          'description': product.description,
          'imageUrl': product.imageUrl,
          'price': product.price,
          'isFavorite': product.isFavorite,
        }),
      );

      final newProduct = Product(
        id: json.decode(response.body)['name'],
        title: product.title,
        description: product.description,
        price: product.price,
        imageUrl: product.imageUrl,
      );

      _items.add(newProduct);
      notifyListeners();
    } catch (err) {
      print(err);
      throw (err);
    }
  }

  Future<void> updateProduct(String id, Product updatedProduct) async {
    final produitIndex = _items.indexWhere((produit) => produit.id == id);
    final url = "https://shop-flutter-3fb65.firebaseio.com/product/$id";

    try {
      if (produitIndex != -1) {
        final response = await http.patch(
          url,
          body: json.encode({
            'title': updatedProduct.title,
            'description': updatedProduct.description,
            'price': updatedProduct.price,
            'imageUrl': updatedProduct.imageUrl,
          }),
        );
        if (response.statusCode >= 400) {
          throw ({'error': '>= 400'});
        }
        _items[produitIndex] = updatedProduct;
        notifyListeners();
      } else {
        print('Product not found !');
      }
    } catch (err) {
      print(err);
      throw (err);
    }
  }

  Future<void> deleteProduct(String id) async {
    final url = "https://shop-flutter-3fb65.firebaseio.com/product/$id";
    final existingProductIndex = _items.indexWhere((prod) => prod.id == id);
    var existingProduct = _items[existingProductIndex];

    _items.removeAt(existingProductIndex);
    notifyListeners();

    final res = await http.delete(url);
    if (res.statusCode >= 400) {
      _items.insert(existingProductIndex, existingProduct);
      notifyListeners();
      throw HttpException('Impossible to delete product: statusCode = ${res.statusCode}').toPrint();
    }
    existingProduct = null;
  }
}
