import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shop/providers/products.dart';
import 'package:shop/screens/admin/edit_product_screen.dart';
import 'package:shop/widgets/admin/user_product_item.dart';
import 'package:shop/widgets/global/app_drawer.dart';

class UserProductScreen extends StatelessWidget {
  static const routeName = '/my-products';

  @override
  Widget build(BuildContext context) {
    final productsData = Provider.of<Products>(context);

    goToAddProduct() {
      Navigator.of(context).pushNamed(EditProductScreen.routeName);
    }

    return Scaffold(
      appBar: AppBar(
        title: const Text('Your Products'),
        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.add),
            onPressed: () => goToAddProduct(),
          )
        ],
      ),
      drawer: AppDrawer(),
      body: RefreshIndicator(
        onRefresh: () => Provider.of<Products>(context).fetchAndSetProduct(),
        child: Padding(
          padding: EdgeInsets.all(8),
          child: ListView.builder(
            itemBuilder: (_, i) => UserProductItem(
              productsData.items[i].id,
              productsData.items[i].title,
              productsData.items[i].imageUrl,
            ),
            itemCount: productsData.items.length,
          ),
        ),
      ),
    );
  }
}
