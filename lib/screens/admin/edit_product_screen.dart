import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../providers/products.dart';
import '../../validators/validator_edit_product.dart';

class EditProductScreen extends StatefulWidget {
  static const routeName = '/edit-product';

  @override
  _EditProductScreenState createState() => _EditProductScreenState();
}

class _EditProductScreenState extends State<EditProductScreen> {
  final _priceFocusNode = FocusNode();
  final _descriptionFocusNode = FocusNode();
  final _imageUrlFocusNode = FocusNode();
  final _imageUrlController = TextEditingController();
  final _form = GlobalKey<FormState>();

  var isInit = true;
  var isLoading = false;

  var _editingProduct = Product(
    id: null,
    title: "",
    price: 0.0,
    description: "",
    imageUrl: "",
  );

  var _initValues = {
    'title': '',
    'price': '',
    'description': '',
  };

  @override
  void initState() {
    _imageUrlFocusNode.addListener(_updateImageUrl);
    super.initState();
  }

  @override
  void didChangeDependencies() {
    if (isInit) {
      final productId = ModalRoute.of(context).settings.arguments as String;
      if (productId != null) {
        _editingProduct = Provider.of<Products>(context, listen: false).findById(productId);
        _initValues = {
          'title': _editingProduct.title,
          'price': _editingProduct.price.toString(),
          'description': _editingProduct.description,
        };
        _imageUrlController.text = _editingProduct.imageUrl;
      }
    }
    isInit = false;
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    _priceFocusNode.dispose();
    _descriptionFocusNode.dispose();
    _imageUrlFocusNode.dispose();
    _imageUrlController.dispose();
    /* _imageUrlFocusNode.removeListener(_updateImageUrl); */
    super.dispose();
  }

  void _updateImageUrl() {
    if (!_imageUrlFocusNode.hasFocus) {
      setState(() {});
      if (!_imageUrlController.text.startsWith('http') && !_imageUrlController.text.startsWith('https')) {
        return;
      }
      if (!_imageUrlController.text.endsWith('.png') && !_imageUrlController.text.endsWith('.jpg') && !_imageUrlController.text.endsWith('.jpeg')) {
        return;
      }
    }
  }

  void _saveProductData(String value, String target) {
    _editingProduct = Product(
      id: target == 'id' ? value : _editingProduct.id,
      title: target == 'title' ? value : _editingProduct.title,
      price: target == 'price' ? double.parse(value) : _editingProduct.price,
      description: target == 'description' ? value : _editingProduct.description,
      imageUrl: target == 'imageUrl' ? value : _editingProduct.imageUrl,
      isFavorite: _editingProduct.isFavorite,
    );
  }

  Future<void> _saveForm() async {
    final isValid = _form.currentState.validate();

    if (!isValid) {
      return;
    }

    _form.currentState.save();
    setState(() {
      isLoading = true;
    });

    try {
      if (_editingProduct.id == null) {
        await Provider.of<Products>(context, listen: false).addProduct(_editingProduct);
      } else {
        await Provider.of<Products>(context, listen: false).updateProduct(_editingProduct.id, _editingProduct);
      }
    } catch (err) {
      await showDialog(
        context: context,
        builder: (ctx) => AlertDialog(
          title: Text('An error occured !'),
          content: Text('Somethin went wrong.'),
          actions: <Widget>[
            FlatButton(
              child: Text('Okay'),
              onPressed: () => Navigator.of(ctx).pop(),
            )
          ],
        ),
      );
    }
    setState(() {
      isLoading = false;
    });
    Navigator.of(context).pop();
  }

  @override
  Widget build(BuildContext context) {
    void _targetRequestFocus(FocusNode target) {
      FocusScope.of(context).requestFocus(target);
    }

    return Scaffold(
      appBar: AppBar(
        title: Text('Edit Product'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.save),
            onPressed: () => _saveForm(),
          ),
        ],
      ),
      body: isLoading
          ? Center(
              child: CircularProgressIndicator(),
            )
          : Padding(
              padding: const EdgeInsets.all(15.0),
              child: Form(
                key: _form,
                child: ListView(
                  children: <Widget>[
                    TextFormField(
                      initialValue: _initValues['title'],
                      decoration: InputDecoration(labelText: 'Title'),
                      textInputAction: TextInputAction.next,
                      onFieldSubmitted: (_) => _targetRequestFocus(_priceFocusNode),
                      validator: (value) => ValidatorEditProduct().checkTitle(value),
                      onSaved: (value) => _saveProductData(value, 'title'),
                    ),
                    TextFormField(
                      initialValue: _initValues['price'],
                      decoration: InputDecoration(labelText: 'Price'),
                      textInputAction: TextInputAction.next,
                      keyboardType: TextInputType.number,
                      focusNode: _priceFocusNode,
                      onFieldSubmitted: (_) => _targetRequestFocus(_descriptionFocusNode),
                      validator: (value) => ValidatorEditProduct().checkPrice(value),
                      onSaved: (value) => _saveProductData(value, 'price'),
                    ),
                    TextFormField(
                      initialValue: _initValues['description'],
                      decoration: InputDecoration(labelText: 'Description'),
                      maxLines: 5,
                      keyboardType: TextInputType.multiline,
                      focusNode: _descriptionFocusNode,
                      validator: (value) => ValidatorEditProduct().checkDescription(value),
                      onSaved: (value) => _saveProductData(value, 'description'),
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: <Widget>[
                        Expanded(
                          child: Container(
                            height: 100,
                            margin: EdgeInsets.only(top: 10, right: 10),
                            decoration: _imageUrlController.text.isEmpty
                                ? BoxDecoration(
                                    border: Border.all(color: Colors.grey),
                                  )
                                : BoxDecoration(),
                            child: _imageUrlController.text.isEmpty
                                ? Container(
                                    alignment: Alignment.center,
                                    child: Text(
                                      'No image ...',
                                      style: TextStyle(color: Colors.grey),
                                    ),
                                  )
                                : FittedBox(
                                    child: Image.network(
                                      _imageUrlController.text,
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                          ),
                        ),
                        Expanded(
                          child: TextFormField(
                            decoration: InputDecoration(labelText: 'Image URL'),
                            keyboardType: TextInputType.url,
                            textInputAction: TextInputAction.done,
                            controller: _imageUrlController,
                            focusNode: _imageUrlFocusNode,
                            validator: (value) => ValidatorEditProduct().checkImageUrl(value),
                            onSaved: (value) => _saveProductData(value, 'imageUrl'),
                            onFieldSubmitted: (_) => _saveForm(),
                          ),
                        )
                      ],
                    )
                  ],
                ),
              ),
            ),
    );
  }
}
