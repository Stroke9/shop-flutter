import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../widgets/global/app_drawer.dart';
import '../../widgets/order/card_order_item.dart';
import '../../providers/orders.dart';

class OrderScreen extends StatelessWidget {
  static const routeName = '/order';

  @override
  Widget build(BuildContext context) {
    print('buildddddd ------ ORDER');
    return Scaffold(
      appBar: AppBar(
        title: Text('Orders'),
      ),
      body: FutureBuilder(
        future: Provider.of<Orders>(context, listen: false).fetchAndSetOrders(),
        builder: (_, dataSnapshot) {
          if (dataSnapshot.connectionState == ConnectionState.waiting) {
            return Center(
              child: CircularProgressIndicator(),
            );
          } else {
            if (dataSnapshot.error == null) {
              return Consumer<Orders>(
                builder: (ctx, ordersData, _) => ListView.builder(
                  itemCount: ordersData.orders.length,
                  itemBuilder: (_, i) => CardOrderItem(ordersData.orders[i]),
                ),
              );
            } else {
              return Center(
                child: Text('An error was occured'),
              );
            }
          }
        },
      ),
      drawer: AppDrawer(),
    );
  }
}
