import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shop/providers/products.dart';

import '../../widgets/global/app_drawer.dart';
import '../../providers/cart.dart';
import '../../screens/cart/cart_screen.dart';
import '../../widgets/cart/badge.dart';
import '../../widgets/product/product_grid.dart';

enum FilterOptions {
  Favorites,
  All,
}

class ProductsOverviewScreen extends StatefulWidget {
  @override
  _ProductsOverviewScreenState createState() => _ProductsOverviewScreenState();
}

class _ProductsOverviewScreenState extends State<ProductsOverviewScreen> {
  var _showFavoriteProducts = false;
  var _isInit = true;
  var _isLoading = false;

  /* @override
  void initState() {
    Future.delayed(Duration.zero).then((_) {
      Provider.of<Products>(context).fetchAndSetProduct();
    });
    super.initState();
  } */

  @override
  void didChangeDependencies() {
    if (_isInit) {
      setState(() {
        _isLoading = true;
      });
      Provider.of<Products>(context).fetchAndSetProduct().then((_) {
        setState(() {
          _isLoading = false;
        });
      });
    }
    _isInit = false;
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    print('!DEBUG: ||| RE-BUILD ||| "/screen/product" -> "product_overview_screen"');
    void goToShoppingCart() {
      Navigator.of(context).pushNamed(CartScreen.routeName);
    }

    return Scaffold(
      appBar: AppBar(
        title: Text('Product Overview'),
        actions: <Widget>[
          PopupMenuButton(
            onSelected: (selectedFilter) {
              setState(() {
                if (selectedFilter == FilterOptions.Favorites) {
                  _showFavoriteProducts = true;
                } else {
                  _showFavoriteProducts = false;
                }
              });
            },
            itemBuilder: (_) => [
              PopupMenuItem(
                child: Text('Show All'),
                value: FilterOptions.All,
              ),
              PopupMenuItem(
                child: Text('View All Favorites'),
                value: FilterOptions.Favorites,
              ),
            ],
          ),
          Consumer<Cart>(
            builder: (_, cart, childCart) => Badge(
              child: childCart,
              value: cart.countItem.toString(),
            ),
            child: IconButton(
              icon: Icon(Icons.shopping_cart),
              onPressed: () => goToShoppingCart(),
            ),
          ),
        ],
      ),
      body: _isLoading
          ? Center(
              child: CircularProgressIndicator(),
            )
          : ProductGrid(_showFavoriteProducts),
      drawer: AppDrawer(),
    );
  }
}
