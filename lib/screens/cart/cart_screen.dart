import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shop/providers/orders.dart';

import '../../providers/cart.dart' show Cart;
import '../../widgets/cart/card_cart_item.dart';

class CartScreen extends StatelessWidget {
  static const routeName = '/cart';

  @override
  Widget build(BuildContext context) {
    print('!DEBUG: ||| RE-BUILD ||| "/screen/cart" -> "cart_screen"');
    final cart = Provider.of<Cart>(context);
    final order = Provider.of<Orders>(context, listen: false);

    return Scaffold(
      appBar: AppBar(
        title: Text('Cart'),
      ),
      body: Column(
        children: <Widget>[
          Card(
            elevation: 5,
            margin: EdgeInsets.all(15),
            child: Padding(
              padding: EdgeInsets.all(10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    'Total',
                    style: TextStyle(fontSize: 20),
                  ),
                  Spacer(),
                  Chip(
                    label: Consumer<Cart>(
                      builder: (_, cart, childChip) => Text(
                        "${cart.totalAmount.toStringAsFixed(2)} €",
                        style: TextStyle(color: Theme.of(context).primaryTextTheme.title.color),
                      ),
                    ),
                    backgroundColor: Theme.of(context).primaryColor,
                  ),
                  OdderButton(
                    cart: cart,
                    order: order,
                  )
                ],
              ),
            ),
          ),
          SizedBox(height: 10),
          Expanded(
            child: Consumer<Cart>(
              builder: (_, cart, childListView) => ListView.builder(
                itemBuilder: (_, i) => CardCartItem(
                  // the id of cardItem contain product(s)
                  cart.items.values.toList()[i].id,
                  cart.items.values.toList()[i].title,
                  cart.items.values.toList()[i].price,
                  cart.items.values.toList()[i].quantity,
                  // the id of product
                  cart.items.keys.toList()[i],
                ),
                itemCount: cart.countItem,
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class OdderButton extends StatefulWidget {
  const OdderButton({
    Key key,
    @required this.cart,
    @required this.order,
  }) : super(key: key);

  final Cart cart;
  final Orders order;

  @override
  _OdderButtonState createState() => _OdderButtonState();
}

class _OdderButtonState extends State<OdderButton> {
  var _isLoading = false;

  @override
  Widget build(BuildContext context) {
    return FlatButton(
      child: _isLoading ? CircularProgressIndicator() : Text('ORDER NOW'),
      textColor: Theme.of(context).accentColor,
      onPressed: widget.cart.totalAmount <= 0 || _isLoading
          ? null
          : () async {
              setState(() {
                _isLoading = true;
              });
              await widget.order.addOrder(
                widget.cart.items.values.toList(),
                widget.cart.totalAmount,
              );
              setState(() {
                _isLoading = false;
              });
              widget.cart.clearCart();
              /* Navigator.of(context).pushNamed(OrderScreen.routeName); */
            },
    );
  }
}
