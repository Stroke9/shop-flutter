class ValidatorEditProduct {
  String checkTitle(String value) {
    return value.isEmpty ? 'Please provide a title' : null;
  }

  String checkPrice(String value) {
    if (double.tryParse(value) == null) {
      return 'Please enter a valid number';
    } else if (value == 'uee') {
      return 'Please provide a number';
    } else if (double.parse(value) <= 0) {
      return 'Please enter a number greater than 0';
    } else {
      return null;
    }
  }

  String checkDescription(String value) {
    if (value.isEmpty) {
      return 'Please enter a description';
    } else if (value.length > 0 && value.length <= 10) {
      return 'Should be at least 10 characters long';
    } else {
      return null;
    }
  }

  String checkImageUrl(String value) {
    if (value.isEmpty) {
      return 'Please entre an image URL';
    }
    if (!value.startsWith('http') && !value.startsWith('https')) {
      return 'Please enter a valid URL';
    }
    if (!value.endsWith('.png') && !value.endsWith('.jpg') && !value.endsWith('.jpeg')) {
      return 'Enter a valid extension (png, jpg, jpeg)';
    }
    return null;
  }
}
