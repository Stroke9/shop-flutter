import 'package:flutter/cupertino.dart';

class ValidatorAuth {

  String checkEmail(String value) {
    if (value.isEmpty || !value.contains('@')) {
      return 'Invalid Email';
    } else {
      return null;
    }
  }

  String checkPassword(String value) {
    if (value.isEmpty || value.length < 5) {
      return 'Password to short';
    } else {
      return null;
    }
  }

  String checkConfirmPassword(String value, TextEditingController pwd) {
    if (value != pwd.text) {
      return 'Password do not match';
    } else {
      return null;
    }
  }
}
