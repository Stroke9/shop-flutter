//TODODelete all debug mod with regex ^.*(!DEBUG).*\n

import 'package:flutter/material.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:shop/providers/auth.dart';
import 'package:shop/screens/auth/auth_screen.dart';
import 'package:provider/provider.dart';

import './providers/orders.dart';
import './providers/products.dart';
import './providers/cart.dart';
import './providers/auth.dart';

import './screens/admin/edit_product_screen.dart';
import './screens/admin/user_product_screen.dart';
import './screens/cart/cart_screen.dart';
import './screens/orders/orders_screen.dart';
import './screens/product/product_details_screen.dart';
import './screens/product/products_overview_screen.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    initializeDateFormatting();
    return MultiProvider(
      providers: [
        ChangeNotifierProvider.value(
          value: Products(),
        ),
        ChangeNotifierProvider.value(
          value: Cart(),
        ),
        ChangeNotifierProvider.value(
          value: Orders(),
        ),
        ChangeNotifierProvider.value(
          value: Auth(),
        )
      ],
      child: MaterialApp(
        title: 'Shop',
        theme: ThemeData(
          primarySwatch: Colors.purple,
          accentColor: Colors.deepOrange,
          fontFamily: 'Lato',
        ),
        home: AuthScreen(),
        routes: {
          ProductDetailScreen.routeName: (_) => ProductDetailScreen(),
          CartScreen.routeName: (_) => CartScreen(),
          OrderScreen.routeName: (_) => OrderScreen(),
          UserProductScreen.routeName: (_) => UserProductScreen(),
          EditProductScreen.routeName: (_) => EditProductScreen(),
        },
      ),
    );
  }
}
