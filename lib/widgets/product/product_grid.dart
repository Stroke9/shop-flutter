import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../widgets/product/product_item.dart';
import '../../providers/products.dart';

class ProductGrid extends StatelessWidget {
  final bool showFavs;

  ProductGrid(this.showFavs);

  @override
  Widget build(BuildContext context) {
    print('!DEBUG: ||| RE-BUILD ||| "/widget/product" -> "product_grid"');
    final productData = Provider.of<Products>(context);
    final products = showFavs ? productData.showFavoriteProduct : productData.items;

    return GridView.builder(
      padding: const EdgeInsets.all(10.0),
      itemCount: products.length,
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 2,
        mainAxisSpacing: 10,
        crossAxisSpacing: 10,
        childAspectRatio: 3 / 2,
      ),
      itemBuilder: (_, i) => ChangeNotifierProvider.value(
        value: products[i],
        child: ProductItem(),
      ),
    );
  }
}
