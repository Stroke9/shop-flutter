import 'package:flutter/material.dart';
import 'package:shop/screens/admin/user_product_screen.dart';
import 'package:shop/screens/orders/orders_screen.dart';

class AppDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    void _goToOtherScreen(routeName) {
      Navigator.of(context).pushReplacementNamed(routeName);
    }

    Widget _builderListTile(String title, IconData icon, String goTo) {
      return ListTile(
        leading: Icon(
          icon,
          size: 30,
          color: Theme.of(context).accentColor,
        ),
        title: Text(title,
            style: TextStyle(
              fontSize: 20,
              color: Colors.black,
            )),
        onTap: () => _goToOtherScreen(goTo),
      );
    }

    return Drawer(
      child: Column(
        children: <Widget>[
          AppBar(
            title: Text('Coucou'),
            automaticallyImplyLeading: false,
          ),
          Divider(),
          _builderListTile('Shop', Icons.shop, '/'),
          _builderListTile('Payment', Icons.payment, OrderScreen.routeName),
          _builderListTile('Manage Products', Icons.edit, UserProductScreen.routeName),
        ],
      ),
    );
  }
}
