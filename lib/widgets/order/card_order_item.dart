import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'dart:math';

import '../../providers/orders.dart';

class CardOrderItem extends StatefulWidget {
  final OrderItem order;

  CardOrderItem(this.order);

  @override
  _CardOrderItemState createState() => _CardOrderItemState();
}

class _CardOrderItemState extends State<CardOrderItem> {
  var _expanded = false;
  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.all(10),
      child: Column(
        children: <Widget>[
          ListTile(
            title: Text('${widget.order.amount} €'),
            subtitle: Text(DateFormat.yMMMd('fr').add_Hms().format(widget.order.dateTime)),
            trailing: IconButton(
              icon: Icon(Icons.expand_more),
              onPressed: () {
                setState(() {
                  _expanded = !_expanded;
                });
              },
            ),
          ),
          if (_expanded)
            Container(
              padding: EdgeInsets.all(20),
              height: min(170, (widget.order.products.length + 1) * 30.0),
              child: ListView(
                children: widget.order.products
                    .map((product) => Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(
                              product.title,
                              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                            ),
                            Text(
                              '${product.quantity} X ${product.price} €',
                              style: TextStyle(fontSize: 18, color: Colors.grey),
                            ),
                          ],
                        ))
                    .toList(),
              ),
            ),
        ],
      ),
    );
  }
}
