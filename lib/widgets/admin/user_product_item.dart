import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../providers/products.dart';
import '../../screens/admin/edit_product_screen.dart';

class UserProductItem extends StatelessWidget {
  final String id;
  final String title;
  final String imageUrl;

  const UserProductItem(this.id, this.title, this.imageUrl);

  _deleteProduct(BuildContext context, ScaffoldState scaffold) async {
    try {
      await Provider.of<Products>(context, listen: false).deleteProduct(id);
      scaffold.showSnackBar(SnackBar(
        content: Text('Deleting succes !'),
      ));
    } catch (err) {
      scaffold.showSnackBar(SnackBar(
        content: Text('Deleting failed !'),
      ));
    }
  }

  @override
  Widget build(BuildContext context) {
    final scaffold = Scaffold.of(context);
    return Column(
      children: <Widget>[
        ListTile(
          leading: CircleAvatar(
            backgroundImage: NetworkImage(imageUrl),
          ),
          title: Text(title),
          trailing: Container(
            width: 100,
            child: Row(
              children: <Widget>[
                IconButton(
                  icon: Icon(Icons.edit),
                  onPressed: () => Navigator.of(context).pushNamed(EditProductScreen.routeName, arguments: id),
                  color: Theme.of(context).primaryColor,
                ),
                IconButton(
                  icon: Icon(Icons.delete),
                  onPressed: () => _deleteProduct(context, scaffold),
                  color: Theme.of(context).errorColor,
                ),
              ],
            ),
          ),
        ),
        Divider(),
      ],
    );
  }
}
